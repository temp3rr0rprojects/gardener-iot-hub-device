# Gardener IoT Hub Device #

Azure cloud I/O communication (D2C and C2D) and CosmosDB storage.
Proportional Integral Derivative (PID) controller: Performs gradual watering using Soil Moisture % readings (https://en.wikipedia.org/wiki/PID_controller).

![alternativetext](https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/670/297/datas/gallery.jpg)

## Techniques & algorithms

- Control Theory
- Proportional Integral Derivative (PID) controller
- Asynchronous Communication: Device2Cloud and Cloud2Device.
- DocumentDB and CosmosDB document storage.

## SDKs & Libraries

- Net.core 2.0
- Azure.Documents
- Threading.Tasks
- Net.Http
- Collections.Generic
- Timers
- Linq
- Json