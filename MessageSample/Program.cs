﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using System;
using System.Text;

namespace Microsoft.Azure.Devices.Client.Samples
{
    public class Program
    {
        private static string s_deviceConnectionString = Environment.GetEnvironmentVariable("IOTHUB_DEVICE_CONN_STRING");
        private static TransportType s_transportType = TransportType.Amqp;
        static DeviceClient deviceClient;

        public static int Main(string[] args)
        {
            args =  new [] { "<secret>" };

            if (string.IsNullOrEmpty(s_deviceConnectionString) && args.Length > 0)
            {
                s_deviceConnectionString = args[0];
            }

            deviceClient = DeviceClient.CreateFromConnectionString(s_deviceConnectionString, s_transportType);
            
            if (deviceClient == null)
            {
                Console.WriteLine("Failed to create DeviceClient!");
                return 1;
            }

            var sample = new MessageSample(deviceClient);

            // TODO: make it a service
            // TODO: push to Windows IoT core

            while(true)
                //sample.RunSampleAsync().GetAwaiter().GetResult();
                sample.ReceiveCommandseAsync().GetAwaiter().GetResult();

            //ReceiveC2dAsync();
            Console.WriteLine("Done.\n");
            return 0;
        }

        private static async void ReceiveC2dAsync()
        {
            Console.WriteLine("\nReceiving cloud to device messages from service");
            while (true)
            {
                Message receivedMessage = await deviceClient.ReceiveAsync();
                if (receivedMessage == null) continue;

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Received message: {0}", Encoding.ASCII.GetString(receivedMessage.GetBytes()));
                Console.ResetColor();

                var msg = Encoding.ASCII.GetString(receivedMessage.GetBytes());
                var msg2 = msg[0];                

                await deviceClient.CompleteAsync(receivedMessage);
            }
        }
    }
}
