﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Timers;
using System.Linq;
using MessageSample.PID;
using Newtonsoft.Json.Linq;

using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

namespace Microsoft.Azure.Devices.Client.Samples
{
    public class MessageSample
    {
        private const int MessageCount = 1;
        private const int TemperatureThreshold = 30;
        private static Random s_randomGenerator = new Random();
        private float _temperature;
        private float _humidity;
        private DeviceClient _deviceClient;
        static Random random = new Random();

        public MessageSample(DeviceClient deviceClient)
        {
            _deviceClient = deviceClient ?? throw new ArgumentNullException(nameof(deviceClient));
        }

        public async Task RunSampleAsync()
        {
            await SendEvent().ConfigureAwait(false);
            await ReceiveCommands().ConfigureAwait(false);
        }

        public async Task ReceiveCommandseAsync()
        {
            await ReceiveCommands().ConfigureAwait(false);
        }

        private async Task SendEvent()
        {
            string dataBuffer;
            Console.WriteLine("Device sending {0} messages to IoTHub...\n", MessageCount);

            for (int count = 0; count < MessageCount; count++)
            {
                _temperature = s_randomGenerator.Next(20, 35);
                _humidity = s_randomGenerator.Next(60, 80);
                dataBuffer = $"{{\"messageId\":{count},\"temperature\":{_temperature},\"humidity\":{_humidity}}}";
                Message eventMessage = new Message(Encoding.UTF8.GetBytes(dataBuffer));
                eventMessage.Properties.Add("temperatureAlert", (_temperature > TemperatureThreshold) ? "true" : "false");
                Console.WriteLine("\t{0}> Sending message: {1}, Data: [{2}]", DateTime.Now.ToLocalTime(), count, dataBuffer);

                await _deviceClient.SendEventAsync(eventMessage).ConfigureAwait(false);
            }
        }

        private async Task ReceiveCommands()
        {
            Console.WriteLine("\nDevice waiting for commands from IoTHub...\n");
            Console.WriteLine("Use the IoT Hub Azure Portal to send a message to this device.\n");

            Message receivedMessage;
            string messageData;

            receivedMessage = await _deviceClient.ReceiveAsync().ConfigureAwait(false);

            if (receivedMessage != null)
            {
                messageData = Encoding.ASCII.GetString(receivedMessage.GetBytes());
                Console.WriteLine("\t{0}> Received message: {1}", DateTime.Now.ToLocalTime(), messageData);

                int propCount = 0;
                double soilHumidityMin = -1;
                double soilHumidityMax = -1;
                foreach (var prop in receivedMessage.Properties)
                {
                    Console.WriteLine("\t\tProperty[{0}> Key={1} : Value={2}", propCount++, prop.Key, prop.Value);

                    if (prop.Key.Equals("soilHumidityMin"))
                    {
                        Console.WriteLine("soilHumidityMin");
                        soilHumidityMin = double.Parse(prop.Value);
                    }
                    if (prop.Key.Equals("soilHumidityMax"))
                    {
                        Console.WriteLine("soilHumidityMax");
                        soilHumidityMax = double.Parse(prop.Value);
                    }
                }

                if (soilHumidityMin != -1 && soilHumidityMax != -1)
                {
                    Console.WriteLine("Sending restful msg");
                    pidRun(soilHumidityMin, soilHumidityMax);

                }
                await _deviceClient.CompleteAsync(receivedMessage).ConfigureAwait(false);
            }
            else
            {
                Console.WriteLine("\t{0}> Timed out", DateTime.Now.ToLocalTime());
            }
        }

        public class DataObject
        {
            public string Name { get; set; }
        }

        static async Task<double> getSoilHumidity()
        {
            double soilHumidity = -1;
            // Example watering: http://espressif/valve_ms?params=500
            const string URL = "http://ha:8123/api/states/sensor.flower1_moisture";
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                string predictionResult = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Result: {0}", predictionResult);
                JObject json = JObject.Parse(predictionResult);

                string stateString = (string)json["state"];
                if (!Double.TryParse(stateString, out soilHumidity))
                    soilHumidity = -1;
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
            client.Dispose();

            return soilHumidity;
        }

        static async Task<double> sendRestfulLedMessage(double timeMs)
        {
            // http://espressif/valve_ms?params=500
            const string URL = "http://espressif/valve_ms";
            string urlParameters = "?params=" + timeMs.ToString();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            double actualMl = -1;
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                string predictionResult = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Sent: {0}",(URL + urlParameters));            
                Console.WriteLine("Result: {0}", predictionResult);
                JObject json = JObject.Parse(predictionResult);

                string stateString = (string)json["return_value"];
                if (!Double.TryParse(stateString, out actualMl))
                    actualMl = -1;
                else
                    Console.WriteLine("Actual mL = {0}", actualMl);
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            client.Dispose();

            return actualMl;
        }


        private static double get_simulated_feedback(double sensor_feedback, double pid_output, int i)
        {
            return ((1 - random.NextDouble() / 8.0) * sensor_feedback + (1 - random.NextDouble() / 8.0) * pid_output - ((1 - random.NextDouble() / 8.0) / (i + 2.0))) * 0.35;
        }


        private static DocumentClient client;

        public class Watering
        {
            /*
             {
            "id": "1",
            "targetMoisture": 17,
            "minMoisture": 35,
            "maxMoisture": 35,
            "setPointMin": 24,
            "setPointMax": 26,
            "waterings": 10,
            "totalWater": 350,
            "proportional": 1.2,
            "integral": 0.9,
            "derivative": 0.4,
            */

            [JsonProperty(PropertyName = "id")]
            public string id { get; set; }
            public double targetMoisture { get; set; }
            public double minMoisture { get; set; }
            public double maxMoisture { get; set; }
            public double setPointMin { get; set; }
            public double setPointMax { get; set; }
            public int waterings { get; set; }
            public double totalWater { get; set; }
            public double proportional { get; set; }
            public double integral { get; set; }
            public double derivative { get; set; }

            public override string ToString()
            {
                return JsonConvert.SerializeObject(this);
            }
        }

        private static async Task<bool> send_to_cosmosdb(Watering item)
        {
            bool success = false;
            string EndpointUri = "https://garderner.documents.azure.com:443/";
            string PrimaryKey = "<secret>";
            string DatabaseId = "<secret>";
            string CollectionId = "<secret>";

            try
            {
                client = new DocumentClient(new Uri(EndpointUri), PrimaryKey);

                await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId), item);

                success = true;
            }
            catch (DocumentClientException de)
            {
                Exception baseException = de.GetBaseException();
                Console.WriteLine("{0} error occurred: {1}, Message: {2}", de.StatusCode, de.Message, baseException.Message);
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
                Console.WriteLine("End of demo, press any key to exit.");
                Console.ReadKey();
            }

            return success;
        }

        private static async void pidRun(double soilHumidityMin, double soilHumidityMax)
        {

            double desired_moisture = (soilHumidityMin + soilHumidityMax) / 2.0;
            double p = 1.2;
            double integral = 0.9;
            double d = 0.4;
            Pid pid = new Pid(p, integral, d);

            double tolerance_percent = 0.05;
            pid.setPoint(desired_moisture, tolerance_percent);
            double desired_moisture_min = desired_moisture * (1 - tolerance_percent);
            double desired_moisture_max = desired_moisture * (1 + tolerance_percent);

            double totalTimeSteps = 30;
            double sensor_feedback = await getSoilHumidity();
            List<double> sensor_readings = new List<double>();
            List<double> outputs = new List<double>();
            List<int> timeSeries = new List<int>();
            List<double> actualMlList = new List<double>();

            if (sensor_feedback < desired_moisture && sensor_feedback != -1)
            {
                Timer timer = new Timer(1000 * 10); // Water every x seconds

                int i = 0;
                timer.Elapsed += async (sender, e) =>
                {
                    double pid_output = pid.update(sensor_feedback);
                    if (pid_output != -1)
                        pid_output *= 30; // TODO: temp 30x
                    double actualMl = await sendRestfulLedMessage(pid_output);
                    actualMlList.Add(actualMl);
                    sensor_readings.Add(sensor_feedback);
                    outputs.Add(pid_output);
                    timeSeries.Add(i);

                    sensor_feedback = await getSoilHumidity();
                    sensor_feedback = get_simulated_feedback(sensor_feedback, pid_output, i); // TODO: restful sensor feedback

                    if (pid_output == 0 || i > totalTimeSteps || outputs.Sum() > 2000 || (sensor_feedback > pid.set_max_point && sensor_feedback < pid.set_max_point))
                    {
                        timer.Stop();
                        for (i = 0; i < timeSeries.Count; i++)
                            Console.WriteLine($"{timeSeries[i]}. sensor_readings: {Math.Round(sensor_readings[i], 2)}, outputs: {Math.Round(outputs[i], 2)}, actual mL {Math.Round(actualMlList[i], 2)}");
                        Console.WriteLine($"Soil Moisture% Readings - Target {desired_moisture} (with {tolerance_percent}% tolerance");
                        Console.WriteLine($"Set Point: Min={Math.Round(desired_moisture_min, 2)}, Set Point: Max={Math.Round(desired_moisture_max, 2)}");
                        Console.WriteLine($"Waterings: {outputs.Count} - Total water: {Math.Round(outputs.Sum(), 2)} mL  - Actual water: {Math.Round(actualMlList.Sum(), 2)} mL- PID(P:{p},I:{integral},D:{d}");

                        // TODO: send to cosmosdb
                        Watering currentWatering = new Watering
                        {
                            id = new DateTime().ToString("yyyyMMdd"),
                            targetMoisture = desired_moisture,
                            minMoisture = soilHumidityMin,
                            maxMoisture = soilHumidityMax,
                            setPointMin = desired_moisture_min,
                            setPointMax = desired_moisture_max,
                            waterings = outputs.Count,
                            totalWater = Math.Round(outputs.Sum(), 2),
                            proportional = p,
                            integral = i,
                            derivative = d
                        };
                        await send_to_cosmosdb(currentWatering);
                    }
                    i++;
                };
                timer.Start();
            }
        }
    }
}
