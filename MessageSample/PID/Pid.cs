﻿namespace MessageSample.PID
{   class Pid
    {
        double Kp;
        double Ki;
        double Kd;
        double Derivator;
        double Integrator;
        double Integrator_min;
        double Integrator_max;
        public double set_min_point;
        public double set_max_point;
        double Tolerance_percent;

        public Pid(double p, double i, double d, double derivator = 0, double integrator = 0, double integrator_max = 500, double integrator_min = -500)
        {
            Kp = p;
            Ki = i;
            Kd = d;
            Derivator = derivator;
            Integrator = integrator;
            Integrator_max = integrator_max;
            Integrator_min = integrator_min;
            set_min_point = 0.0;
            set_max_point = 0.0;
        }

        public double update(double current_value)
        {
            double error = (set_min_point + set_max_point) / 2.0 - current_value;
            double PID = 0;

            if (!(set_min_point < current_value && current_value < set_max_point))
            {
                double P_value = Kp * error;
                double D_value = Kd * (error - Derivator);
                Derivator = error;
                Integrator = Integrator + error;
                if (Integrator > Integrator_max)
                    Integrator = Integrator_max;
                if (Integrator < Integrator_min)
                    Integrator = Integrator_min;
                double I_value = Integrator * Ki;
                PID = P_value + I_value + D_value;
            }
            return PID;
        }

        public void setPoint(double set_point, double tolerance_percent)
        {
            set_min_point = set_point * (1 - tolerance_percent);
            set_max_point = set_point * (1 + tolerance_percent);
            Integrator = 0;
            Derivator = 0;
        }
    }
}